<?php

namespace AppBundle\Service\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class TransactionTest extends WebTestCase
{
    
   
    public function testAllCategories()
    {

        $client = static::createClient();
        $transactionService =  $client->getKernel()->getContainer()->get('app.transactionUtilityService');
        
        //php bin/console transaction showTransactions valute.csv date 01/04/2015 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'date', '01/04/2015', 'to-eur' , false );               
        $this->assertEquals( 'OK', trim( $resp ) );
        
        //php bin/console transaction showTransactions valute.csv customer 1 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'customer', '1', 'to-eur' , false );        
        $this->assertEquals( 'OK', trim( $resp ) );
        
        //php bin/console transaction showTransactions valute.csv value £50.00 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'value', '£50.00', 'to-eur' , false );        
        $this->assertEquals( 'OK', trim( $resp ) );
        
        
        //php bin/console transaction showTransactions valute.csv date 01/04/2015 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'date', '01/04/2018', 'to-eur' , false );               
        $this->assertEquals( 'KO', trim( $resp ) );
        
        //php bin/console transaction showTransactions valute.csv customer 1 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'customer', '3', 'to-eur' , false );        
        $this->assertEquals( 'KO', trim( $resp ) );
        
        //php bin/console transaction showTransactions valute.csv value £50.00 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'value', '£55.00', 'to-eur' , false );        
        $this->assertEquals( 'KO', trim( $resp ) ); 
        
        //php bin/console transaction showTransactions valute.csv value £50.00 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.cssv', 'value', '£55.00', 'to-eur' , false );        
        $this->assertEquals( 'KO', trim( $resp ) ); 
        
        //php bin/console transaction showTransactions valute.csv value £50.00 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'values', '£55.00', 'to-eur' , false );        
        $this->assertEquals( 'KO', trim( $resp ) ); 
        
        //php bin/console transaction showTransactions valute.csv value £50.00 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', '', 'values', '£55.00', 'to-eurs' , false );        
        $this->assertEquals( 'KO', trim( $resp ) ); 
        
        //php bin/console transaction showTransactions valute.csv value £50.00 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'value', '£55.00', false , false );        
        $this->assertEquals( 'KO', trim( $resp ) ); 
        
        //php bin/console transaction showTransactions valute.csv value £50.00 --convert=to-eur
        $resp = $transactionService->run( 'showTransactions', 'valute.csv', 'value', '', false , false );        
        $this->assertEquals( 'KO', trim( $resp ) ); 
        
    }        
}

//https://symfony.com/doc/3.4/testing.html
//http://api.symfony.com/3.4/Symfony/Component/DomCrawler/Crawler.html#method_first