<?php

namespace AppBundle\Service\TransactionService;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Twig_Environment as Environment;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;
use AppBundle\Service\FileService\FileUtilityService;


class TransactionUtilityService {

    private $twig                       = false;
    private $requestStack               = false;
    private $container                  = false;
    private $entityManager              = false;
    private $action, $file, $customer   = false;
    private $basePath                   = 'data/';
    
    /**
     * Metodo costruttore
     * @param \AppBundle\Service\AppService\Environment $twig ( riferimento al template TWIG )
     * @param \AppBundle\Service\AppService\RequestStack $requestStack ( stack delle richieste )
     * @param \AppBundle\Service\AppService\Container $container ( riferimento contenitore symfony )
     * @param \AppBundle\Service\AppService\EntityManager $entityManager ( database )
     */
    public function __construct( Environment $twig, RequestStack $requestStack, Container $container, EntityManager $entityManager ) {
        $this->twig             = $twig;
        $this->requestStack     = $requestStack;
        $this->container        = $container;
        $this->entityManager    = $entityManager;
    }
 
    /**
     * Funzione che determina il metodo da chiamare in base all'azione definita all'utente in riga di comando
     * @param type $action ( Azione da lanciare )
     * @param type $file ( Nome del file da leggere )
     * @param type $customer ( Id Customer da leggere)
     * return string
     */
    public function run( $action, $file, $field, $search, $convertValue = false, $printResult = true ) {
        $this->action       = $action;
        $this->file         = $file;
        $this->field        = $field;
        $this->search       = $search;
        $this->convertValue = $convertValue;
        $this->printResult  = $printResult;
        
        switch( $action ) {
            case 'showTransactions';
                return $this->showTransactions();
            break;
        }
    }
    
    /**
     * Avvia l'elaborazione del file spcificato, e stampa il risultato finale
     */
    private function showTransactions() {
        //Instanzia il sevizio per la lettura dei file
        $fileUtilityService = $this->container->get( 'app.fileUtilityService' );        
        $currencyService = $this->container->get( 'app.currencyService' );        
        
        //Legge il file con il metodo spcifico
        $filePath = $this->basePath.$this->file;        
        if( !empty( $this->printResult ) ) {
            echo "LETTURA FILE: $filePath\n";
        }
        $oResponse = $fileUtilityService->readCSV( $filePath );        
        
        //Chiama il metodo della classe per filtrare i risultati per il campo specificato a riga di comando
        $oFilter = $this->filterBy( $oResponse, $this->field, $this->search );
        
        if( empty( $oFilter ) ) {
            if( !empty( $this->printResult ) ) {
                echo "Nessun risultato per il filtro inserito: $this->field == $this->search\n";
                return;
            } else {
                return 'KO';
            }
            
        }
        
        $filterResult = array();
        //Cicla tutti gli item 
        foreach( $oFilter AS $filters ) {
            $customerService    = new \AppBundle\Service\TransactionService\CustomerService( $currencyService );
            
            //Cicla tutti i campi di un elemento
            foreach( $filters AS $field => $filter ) {
                
                //Crea il nome del metodo della classe customer
                $set = 'set'.ucfirst( $field );                
                
                //Se esiste il metodo per la classe effettua il set con il valure corrente
                if( method_exists( $customerService, $set) ) {                                      
                    $customerService->{$set}( $filter, $this->convertValue );
                }
            }
            
            //Aggiunge l'oggetto valorizzato customer all'array dei risultati
            $filterResult[] = $customerService;            
            unset( $customerService );
        }
        
        if( !empty( $this->printResult ) ) {
            echo "FILTRO: $this->field == $this->search\n";
        }
        return $this->printResult( $filterResult );
    }
    
     /**
     * Metodo che avvia filtra i risultati per il campo e il valore specificato
     * @param object $oResult ( Oggetto con tutti i risultati da filtrare )
     * @return int $field ( Nome del campo per cui filtrare )
     * @return int $value ( Valore per cui effettuare il filtro )
     */
    public function filterBy( $oResult, $field, $value ) {
        $aFiler = array();
        
        //Cicla i risultati
        foreach( $oResult AS $result ) {
            //Se non è presente il campo ricercato ritorna false
            if( empty( $result->{$field} ) ) {
                return false;
                break;
            }
            
            //Se il campo ricercato è uguale al valore richiesto lo aggiunge all'array dei risultati
            if( $result->{$field} == $value ) {
                $aFiler[] = $result;
            }
        }
        
        $aFiler = json_decode(json_encode($aFiler), FALSE);
        return $aFiler;
    }
    
    
    private function printResult( $results ) {
        if( !empty( $this->printResult ) ) {
            echo "\n| N. | Customer ID | Data       | Value \n";
            foreach( $results AS $key => $result ) {
                echo "| $key  | ".$result->getCustomer()."           | ".$result->getDate()." | ".$result->getValue()."\n";
            }        
            echo "\n";
        } else {
            return 'OK';
        }
    }
    
    
}