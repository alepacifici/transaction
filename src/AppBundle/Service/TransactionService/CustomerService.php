<?php

namespace AppBundle\Service\TransactionService;
use AppBundle\Service\CurrencyService\CurrencyService;

class CustomerService {
    private $currencyService    = false;
    private $customer           = false;
    private $date               = false;
    private $value              = false;
   
    /**
     * Metodo costruttore     
     */
    public function __construct( CurrencyService $currencyService) {
        $this->currencyService = $currencyService;
    }
 
    /**
     * Metodo che ritorna il valore del customer
     * return int $customer
     */
    public function getCustomer() {
        return $this->customer;
    }
    
    /**
     * Metodo che ritorna il valore della data
     * return date $date
     */
    public function getDate() {
        return $this->date;
    }
    
    /**
     * Metodo che ritorna il valore della data
     * return float value
     */
    public function getValue() {
        return $this->value;
    }
    
    /**
     * Effettua il set del valore del customer sulla variabile di classe
     * @param int $customer
     */
    public function setCustomer( $customer ) {
        $this->customer = $customer;
    }
    
    /**
     * Effettua il set del valore della data sulla variabile di classe
     * @param date $date
     */
    public function setDate( $date ) {
        $this->date = $date;
    }
    
    /**
     * Effettua il set del valore dell value sulla variabile di classe
     * @param date $value
     */
    public function setValue( $value, $convertValue = false ) {        
        $this->value = !empty( $convertValue ) && !empty( $this->currencyService ) ? $this->currencyService->convertValue( $value, $convertValue ) : $value;
    }
    
    
    
}