<?php

namespace AppBundle\Service\FileService;

class FileUtilityService {
   
    /**
     * Metodo costruttore     
     */
    public function __construct() {
        
    }
 
    
    /**
     * Metodo che avvia la lettura di un file CSV e ritorna un oggetto ordinato associativamente
     * @param type $filePath
     * @return type
     */
    public function readCSV( $filePath ) {
       
        $response = array();
        
        //Avvio lettura e ciclo file
        if ( ( $handle = fopen( $filePath, "r" ) ) !== FALSE ) {
            $x = 0;
            //Se è la prima riga delle intestazioni recupero tutto le label per rendere gli array associativi
            if( $x == 0 ) {
                $labels = $data = fgetcsv($handle, 1000, ";");
            }
            
            //Cicla tutte le righe del file
            $index = 0;
            while ( ( $data = fgetcsv( $handle, 1000, ";" ) ) !== FALSE ) {                   
                foreach( $data AS $key => $item ) {
                    //Aggiunge i nuovi elementi nell'array
                    $response[$index][$labels[$key]] = $item;
                }
                $index++;
            }
        }
        
        //Converto array in oggetto
        $response = json_decode(json_encode($response), FALSE);
        return $response;
    }
    
    
    
}