<?php

namespace AppBundle\Service\CurrencyService;


class CurrencyService {
   
    /**
     * Metodo costruttore     
     */
    public function __construct() {
        
    }
 
    /**
     * Metodo che ritorna il valore del customer
     * return float $customer
     */
    public function convertValue( $value, $convert = false ) {
        if( empty( $convert ) || $convert == 'false' ) {
            return $value;
        }
        
        switch( $convert ) {
            case 'to-eur':
                return $this->convertToEur( $value );
            break;
        }        
    }
    
    /**
     * Metodo che avvia la conversione di tutti i value in euro
     * @param float $eur
     */
    private function convertToEur( $value ) {
        //recupera il primo carattere facendo l'encoding in UTF-8
        $symbol = mb_substr($value,0,1,'UTF-8');
        
        switch( $symbol ) {
            case '£': 
                $eur = '€'.$this->convertPoundToEur( trim( $value, $symbol ) );
            break;
            case '$':               
                $eur = '€'.$this->convertDollarToEur( trim( $value, $symbol ) );
            break;
            default:
                $eur = $value;
            break;
        }        
        return $eur;
    }
    
    /**
     * Converted i pound in euro simulato
     * @param float $eur
     */
    public function convertPoundToEur( $pound ) {         
        $eur =  ( $pound / 100 ) * 110;
        return round( $eur, 2 );
    }
    
    /**
     * Converte i dollari in euro simulato
     * @param float $eur
     */
    public function convertDollarToEur( $dollar ) {
        $eur =  ( $dollar / 100 ) * 55;
        return round( $eur, 2 );
    }
    
 
    
}
