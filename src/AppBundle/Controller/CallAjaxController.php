<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Storage\Handler\MemcachedSessionHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;

class CallAjaxController extends TemplateController {

    /**
     * @Route("/widget")
     */
    public function widgetAction(Request $request) {
        return new Response($this->compressHtml($this->renderSingleTemplate($request)));

        if ($this->checkSetResponseCache($request)) { 
            $ttl = $this->getTTLCacheResponse($request->query->get('widget'));

            $eTag = md5($request->server->get('REQUEST_URI')) . '?v=' . $this->container->getParameter('app.eTagVersion');
//            $d = new \DateTime();
//            $date = $d->createFromFormat( 'Y-m-d', $request->query->get( 'date' ) );

            $response = new Response();
            $response->setETag($eTag);
            $response->setPublic();

            //se esiste la copia cachata la restituisce
            if ($response->isNotModified($request)) {
                $response->setNotModified();
                return $response;
            } else {
                //recupera la risposta e la setta in cache
                $response = new Response($this->renderSingleTemplate($request));
                $response->setETag($eTag);
                $response->setPublic();
                $response->setMaxAge($ttl);
                $response->setSharedMaxAge($ttl);
//                $response->headers->addCacheControlDirective('must-revalidate', true);

                return $response;
            }
        } else {
            return new Response($this->renderSingleTemplate($request));
        }
    }

    /**
     * @Route("/dataWidget")
     */
    public function dataWidgetAction(Request $request) {
        if ($this->checkSetResponseCache($request)) {
            $ttl = $this->getTTLCacheResponse($request->query->get('widget'));
//            
//            $this->memcached  = new \Memcached( $this->container->getParameter( 'session_memcached_persistent_id' ) );
//            // Add server if no connections listed. 
//            if ( !count($this->memcached->getServerList()) ) {            
//                $this->memcached->addServer( $this->container->getParameter( 'session_memcached_host' ), $this->container->getParameter( 'session_memcached_port' ) );  
//            }
            
            $cacheUtility = $this->container->get('app.cacheUtility');
            $cacheUtility->initPhpCache();
            
            $eTag = md5($request->server->get('REQUEST_URI')) . '?v=' . $this->container->getParameter('app.eTagVersion');

            $data = $cacheUtility->phpCacheGet($this->container->getParameter('session_memcached_prefix') . 'dataWidget_' . $eTag);
            if (empty($data)) {
                $data = json_encode($this->getDataCoreWidget($request));
                $cacheUtility->phpCacheSet($this->container->getParameter('session_memcached_prefix') . 'dataWidget_' . $eTag, $data, $ttl);
            }
            $cacheUtility->closePhpCache();
        } else {
            $data = json_encode($this->getDataCoreWidget($request));
        }        
        return new Response($data);
    }

    /**
     * @Route("/user/login")
     */
    public function userLogin(Request $request) {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
                ->findOneByEmail( $request->get("email") );

        if (empty($user)) {
            return new Response(0);
        } else {
            $session = new Session();
            $session->set( 'user', $request->get("email") );

            $cacheUtility = $this->container->get('app.cacheUtility');
            $cacheUtility->initPhpCache();
            
            $cacheUtility->phpCacheSet( md5 ( $this->container->getParameter('s_memcached_prefix_userdata').$request->get( "email" ) ), $user );  
            $cacheUtility->closePhpCache();
            return new Response(1);
        }
    }

    /**
     * @Route("/user/register")
     */
    public function userRegister(Request $request) {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
                ->findOneByEmail($request->get("email"));

        if (empty($user)) {

            $user = new User();
            $user->setName($request->get("name"));
            $user->setSurname($request->get("surname"));
            $user->setEmail($request->get("email"));
            $user->setPassword($request->get("password"));
            $user->setAge($request->get("age"));
            $user->setCity($request->get("city"));
            $user->setTel($request->get("tel"));
            $user->setTeam($request->get("team"));

            $em = $this->getDoctrine()->getManager();

            // tells Doctrine you want to (eventually) save the Product (no queries yet)
            $em->persist($user);

            // actually executes the queries (i.e. the INSERT query)
            $em->flush();

            if (empty($user->getId())) {
                return new Response(2);
            } else {
                return new Response(1);
            }
        }

        return new Response(0);
    }
    /**
     * @Route("/user/logout", name="userlogout")
     */
    public function userLogout(Request $request) {
        $session = new Session();
        $session->remove( 'user' );        
        header( 'Location: /' );
    
        return $this->redirectToRoute( 'homepage');
    }

}
