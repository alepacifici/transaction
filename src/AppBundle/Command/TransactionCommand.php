<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TransactionCommand extends ContainerAwareCommand {
    
    protected function configure() {
        $this->setName( 'transaction' )
             ->addArgument( 'action', InputArgument::REQUIRED, 'Azione da eseguire' )
             ->addArgument( 'file', InputArgument::REQUIRED, 'Nome del file da leggere' )
             ->addArgument( 'field', InputArgument::REQUIRED, 'Campo su cui cercare' )
             ->addArgument( 'search', InputArgument::REQUIRED, 'Search' )
             ->addOption('convert', null, InputOption::VALUE_OPTIONAL, 'conver', '' )
             ->setDescription( 'Avvia elaborazione valute');
    }
    
    /**
     * Execute the command asking the proper controller to parse the given feed.
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     * @return NULL
     */
    protected function execute( InputInterface $input, OutputInterface $output ) {
        $transactionService     = $this->getContainer()->get( 'app.transactionUtilityService' );
        $action                 = $input->getArgument( 'action' );
        $file                   = $input->getArgument( 'file' );
        $field                  = $input->getArgument( 'field' );
        $search                 = $input->getArgument( 'search' );
        $convert                = $input->getOption( 'convert' );
        
        try {
            $transactionService->run( $action, $file, $field, $search, $convert );
        } catch(Exception $e) {
            $output->writeln('Exception thrown with code ' . $e->getCode() . ': ' . $e->getMessage());
        }
    }

}
